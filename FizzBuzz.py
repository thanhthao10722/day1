def FizzBuzz(n):
    array = []
    for i in range(1, n + 1):
        if i % 15 == 0:
            array.append('FizzBuzz')
            
        if i % 3 == 0:
            array.append('Fizz')
           
        if i % 5 == 0:
            array.append('Buzz')
           
        else:
             array.append(str(i))
           
    return array
print(FizzBuzz(16))
